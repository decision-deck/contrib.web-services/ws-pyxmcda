#! /usr/bin/env python2.7
'''
Transforms a file containing categories profiles from a comma-separated values (CSV) file to a XMCDA compliant file, containing the alternatives ids with their limits (lowerCategory and upperCategory)'''
import argparse, csv, sys, os
from optparse import OptionParser

# not using PyXMCDA, to avoid the unnecessary dependency to lxml
def xmcda_write_header(xmlfile) :
    xmlfile.write("<?xml version='1.0' encoding='UTF-8'?>\n")
    xmlfile.write("<xmcda:XMCDA xmlns:xmcda='http://www.decision-deck.org/2009/XMCDA-2.1.0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.decision-deck.org/2009/XMCDA-2.1.0 http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.1.0.xsd'>\n")


def xmcda_write_footer(xmlfile) :
    xmlfile.write("</xmcda:XMCDA>\n")


def xmcda_write_method_messages(xmlfile, type, messages) :
    if type not in ('log', 'error'):
        raise ValueError, 'Invalid type: %s' % type
    xmlfile.write('<methodMessages>\n')
    for message in messages :
        xmlfile.write('<%sMessage><text><![CDATA[%s]]></text></%sMessage>\n' % (type, message, type))
    xmlfile.write('</methodMessages>\n')


def csv_reader(csv_file):
    csvfile = open(csv_file, "rb")
    dialect = csv.Sniffer().sniff(csvfile.read(1024))
    csvfile.seek(0)
    return csv.reader(csvfile, dialect)

def transform(csv_file):
    try:
        content = csv_reader(csv_file)
    except:
        raise ValueError, 'Could not read csv file'
    
    alternatives_limits = []

    for line in content:
        if len(line)!=3:
            raise ValueError, 'Invalid csv file at line %i (3 cells per line)'%content.line_num
        alt_id = line[0].strip()
        if alt_id=='':
            raise ValueError, 'Invalid csv file at line %i (1st cell cannot be empty)'%content.line_num
        lower = line[1].strip()
        upper = line[2].strip()
        if lower == '' or upper == '':
            raise ValueError, 'Invalid csv file at line %i (both lower and upper bounds are mandatory)'%content.line_num

        alternatives_limits.append((alt_id, lower, upper))
        
    if len(alternatives_limits) == 0:
        raise ValueError, 'Invalid csv file (nothing in it??)'

    return alternatives_limits

CATEGORY_PROFILE_XMCDA='''\
    <categoryProfile>
      <alternativeID>%s</alternativeID>
      <limits>
        <lowerCategory>
          <categoryID>%s</categoryID>
        </lowerCategory>
        <upperCategory>
          <categoryID>%s</categoryID>
        </upperCategory>
      </limits>
    </categoryProfile>
'''

def output_categoriesProfiles(filename, alternatives_limits):
    outfile = open(filename, 'w')
    xmcda_write_header(outfile)
    outfile.write('  <categoriesProfiles>\n')

    for alt_limit in alternatives_limits:
        outfile.write(CATEGORY_PROFILE_XMCDA%alt_limit)

    outfile.write('  </categoriesProfiles>\n')
    xmcda_write_footer(outfile)
    outfile.close()

def csv_to_categoriesProfiles(csv_file, out_categoriesProfiles):
    # If some mandatory input files are missing
    if not os.path.isfile(csv_file):
        raise ValueError, "input file '%s' is missing"%csv_file
    alternatives_limits = transform(csv_file)
    output_categoriesProfiles(out_categoriesProfiles, alternatives_limits)


def main(argv=None):
    if argv is None:
        argv = sys.argv
    
    parser = argparse.ArgumentParser(description=__doc__)

    grp_input = parser.add_mutually_exclusive_group(required=True)
    grp_input.add_argument('-I', '--in-dir')
    grp_input.add_argument('-i', '--csv')

    grp_output = parser.add_argument_group("Outputs",
                                           description="Options -f and -O are mutually exclusive")
    grp_output.add_argument('-O', '--out-dir', metavar='<output directory>', help='If specified, the file "categoriesProfiles.xml" will be created in this directory.  The directory must exist beforehand.')
    grp_output.add_argument('-c', '--categoriesProfiles', metavar='categoriesProfiles.xml')

    grp_output.add_argument('-m', '--messages', metavar='<file.xml>', help='All messages are redirected to this XMCDA file instead of being sent to stdout or stderr.  Note that if an output directory is specified (option -O), the path is relative to this directory.')

    args = parser.parse_args()

    if args.out_dir and args.categoriesProfiles:
        parser.error('Options -O and -c are mutually exclusive')
    if args.categoriesProfiles is None and args.out_dir is None:
        parser.error('Either -c or -O must be supplied')

    if args.in_dir:
        csv_file = os.path.join(args.in_dir, 'categoriesProfiles.csv')
    else:
        csv_file = args.csv

    if args.out_dir:
        out_categoriesProfiles = os.path.join(args.out_dir,
                                              'categoriesProfiles.xml')
    else:
        out_categoriesProfiles = args.categoriesProfiles

    if args.messages and args.out_dir is not None:
        args.messages = os.path.join(args.out_dir, args.messages)

    if args.messages:
        args.messages_fd = open(args.messages, 'w')
        xmcda_write_header(args.messages_fd)

    exitStatus = 0
    try:
        csv_to_categoriesProfiles(csv_file, out_categoriesProfiles)
    except ValueError as e:
        exitStatus = -1
        if args.messages:
            xmcda_write_method_messages(args.messages_fd, 'error', e.args)
        else:
            sys.stderr.write('\n'.join(e.args))
    else:
       if args.messages: xmcda_write_method_messages(args.messages_fd, 'log', ['Execution ok'])
    finally:
        if args.messages:
            xmcda_write_footer(args.messages_fd)
            args.messages_fd.close()

    return exitStatus


if __name__ == "__main__":
    sys.exit(main())
