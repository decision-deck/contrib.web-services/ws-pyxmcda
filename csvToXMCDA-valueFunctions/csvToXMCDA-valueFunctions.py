#! /usr/bin/env python2.7
'''
Transforms a file containing value functions from a comma-separated values (CSV) file to a XMCDA compliant file.'''
import argparse, csv, sys, os
from optparse import OptionParser

# not using PyXMCDA, to avoid the unnecessary dependency to lxml
def xmcda_write_header(xmlfile) :
    xmlfile.write("<?xml version='1.0' encoding='UTF-8'?>\n")
    xmlfile.write("<xmcda:XMCDA xmlns:xmcda='http://www.decision-deck.org/2009/XMCDA-2.1.0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.decision-deck.org/2009/XMCDA-2.1.0 http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.1.0.xsd'>\n")


def xmcda_write_footer(xmlfile) :
    xmlfile.write("</xmcda:XMCDA>\n")


def xmcda_write_method_messages(xmlfile, type, messages) :
    if type not in ('log', 'error'):
        raise ValueError, 'Invalid type: %s' % type
    xmlfile.write('<methodMessages>\n')
    for message in messages :
        xmlfile.write('<%sMessage><text><![CDATA[%s]]></text></%sMessage>\n' % (type, message, type))
    xmlfile.write('</methodMessages>\n')


def csv_reader(csv_file):
    csvfile = open(csv_file, "rb")
    try:
        dialect = csv.Sniffer().sniff(csvfile.read(1024))
    except:
        csvfile.seek(0)
        dialect = csv.Sniffer().sniff(csvfile.read(1024))
    csvfile.seek(0)
    return csv.reader(csvfile, dialect)

def normalize_numeric_list(alist):
    """
    Check that the list is made of numeric values only.  If the values in the
    list are not valid numeric values, it also tries to interpret them with
    the comma character (",") as the decimal separator.  This may happen when
    the csv is exported by MS Excel on Windows platforms, where the csv format
    depends on the local settings.

    Note that we do not check whether the decimal separator is the same
    everywhere: a list containing "4.5" and "5,7" will be accepted.

    Return the same list as the supplied ones, except that every comma is
    replaced by a point character.
    It raises ValueError if at least one value could not be interpreted as a
    numeric value.
    """
    # try with ',' as a comma separator on strings
    alist = [ i.replace(',', '.') if type(i) in (str, unicode) else i
              for i in alist ]

    try:
        [ float(i) for i in alist ]
    except ValueError:
        raise ValueError, "Invalid literal for float"

    return alist

def id_name(raw_id):
    """
    Extract from the raw id (i.e. the one directly read in the csv file)
    the name supplied between parenthesis, if any.
    Return a tuple (id, name), and name is None if it is not available.
    """
    _id = raw_id
    _name = None
    if '(' in raw_id and raw_id.endswith(')'):
        _id, _name = raw_id.split('(',2)
        _id = _id.strip()
        _name=_name[:-1]
    return _id, _name

def transform(csv_file):
    try:
        content = csv_reader(csv_file)
    except Exception as e:
        raise ValueError, 'Could not read csv file (%s)'%(e.args[0] if e.args else 'Unknown reason')
    
    criteria_ids = []
    criteria_names = []
    points = {} # criteria_id -> [ point1, point2 ] w/ point=(x,y)

    for line in content:
        if len(line)!=3 and len(line)!=4:
            raise ValueError, 'Invalid csv file at line %i (three or four cells per line)'%content.line_num
        id_name_are_comma_separated = len(line)==4
        raw_id = line[0].strip()
        if raw_id=='' and len(criteria_ids) == 0:
            raise ValueError, 'Invalid csv file (1st line must contain a criterion id in the 1st cell)'

        if raw_id != '':
            if id_name_are_comma_separated:
                crit_id = raw_id
                crit_name = line[1]
            else:
                crit_id, crit_name = id_name(raw_id)
            current_criteria_id = crit_id
            criteria_ids.append(crit_id)
            criteria_names.append(crit_name)

        # append the two last points
        point = normalize_numeric_list( line[-2:] )
        points.setdefault(criteria_ids[-1], []).append(point)

    if len(criteria_ids) == 0:
        raise ValueError, 'Invalid csv file (nothing in it??)'

    return criteria_ids, criteria_names, points

POINT_XMCDA='''\
          <point>
            <abscissa>
              <real>%s</real>
            </abscissa>
            <ordinate>
              <real>%s</real>
            </ordinate>
          </point>
'''

def output_valueFunctions(filename, criteria_ids, criteria_names, points):
    outfile = open(filename, 'w')
    xmcda_write_header(outfile)
    outfile.write('  <criteria mcdaConcept="criteria">\n')
    for _id, name in map(None, criteria_ids, criteria_names):
        if name is not None:
            name = ' name="%s"'%name
        else:
            name = ''
        outfile.write('    <criterion id="%s"%s>\n' % (_id, name))
        outfile.write('      <active>true</active>\n')
        outfile.write('      <criterionFunction>\n')
        outfile.write('        <points>\n')
        for point in points[_id]:
            outfile.write(POINT_XMCDA%tuple(point))

        outfile.write('        </points>\n')
        outfile.write('      </criterionFunction>\n')
        outfile.write('    </criterion>\n')

    outfile.write('  </criteria>\n')
    xmcda_write_footer(outfile)
    outfile.close()

def csv_to_valueFunctions(csv_file, out_valueFunction):
    # If some mandatory input files are missing
    if not os.path.isfile(csv_file):
        raise ValueError, "input file '%s' is missing"%csv_file
    criteria_ids, criteria_names, points = transform(csv_file)
    output_valueFunctions(out_valueFunction,
                          criteria_ids, criteria_names, points)


def main(argv=None):
    if argv is None:
        argv = sys.argv
    
    parser = argparse.ArgumentParser(description=__doc__)

    grp_input = parser.add_mutually_exclusive_group(required=True)
    grp_input.add_argument('-I', '--in-dir')
    grp_input.add_argument('-i', '--csv')

    grp_output = parser.add_argument_group("Outputs",
                                           description="Options -f and -O are mutually exclusive")
    grp_output.add_argument('-O', '--out-dir', metavar='<output directory>', help='If specified, the file "valueFunctions.xml" will be created in this directory.  The directory must exist beforehand.')
    grp_output.add_argument('-f', '--valueFunctions', metavar='valueFunctions.xml')

    grp_output.add_argument('-m', '--messages', metavar='<file.xml>', help='All messages are redirected to this XMCDA file instead of being sent to stdout or stderr.  Note that if an output directory is specified (option -O), the path is relative to this directory.')

    args = parser.parse_args()

    if args.out_dir and args.valueFunctions:
        parser.error('Options -O and -f are mutually exclusive')
    if args.valueFunctions is None and args.out_dir is None:
        parser.error('Either -f or -O must be supplied')

    if args.in_dir:
        csv_file = os.path.join(args.in_dir, 'valueFunctions.csv')
    else:
        csv_file = args.csv

    if args.out_dir:
        out_valueFunctions = os.path.join(args.out_dir, 'valueFunctions.xml')
    else:
        out_valueFunctions = args.valueFunctions

    if args.messages and args.out_dir is not None:
        args.messages = os.path.join(args.out_dir, args.messages)

    if args.messages:
        args.messages_fd = open(args.messages, 'w')
        xmcda_write_header(args.messages_fd)

    exitStatus = 0
    try:
        csv_to_valueFunctions(csv_file, out_valueFunctions)
    except ValueError as e:
        exitStatus = -1
        if args.messages:
            xmcda_write_method_messages(args.messages_fd, 'error', e.args)
        else:
            sys.stderr.write('\n'.join(e.args))
    else:
       if args.messages: xmcda_write_method_messages(args.messages_fd, 'log', ['Execution ok'])
    finally:
        if args.messages:
            xmcda_write_footer(args.messages_fd)
            args.messages_fd.close()

    return exitStatus


if __name__ == "__main__":
    sys.exit(main())
